# Generated by Django 3.2 on 2022-06-04 16:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0013_alter_doctortime_date'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='DoctorTime',
            new_name='DoctorDailyTime',
        ),
    ]
