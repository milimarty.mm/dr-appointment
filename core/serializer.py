from datetime import date

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from core.models import DoctorDailyTime, Appointment, Patient
from utils.inline_serializer import inline_serializer


class PatientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Patient
        fields = ["id", "name", "phone_number"]


class DoctorTimeAppointmentSerializer(serializers.ModelSerializer):
    patient = PatientSerializer(read_only=True)

    class Meta:
        model = Appointment
        fields = ["id", "appointment_date", "reserved_date", "status", "patient"]


class AppointmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Appointment
        fields = ["id", "appointment_date", "status"]


class CreatePatientSerializer(serializers.ModelSerializer):
    def validate(self, data):
        if data.get('appointment') is None:
            raise ValidationError(detail={'appointment': 'appointment is required'})
        return data

    class Meta:
        model = Patient
        fields = ["id", "name", "phone_number", "appointment"]
        extra_kwargs = {
            'appointment': {'write_only': True}
        }


class DoctorDailyTimeSerializer(serializers.ModelSerializer):
    class Meta:
        model = DoctorDailyTime
        fields = ["id", "start_time", "end_time", "date", "appointments"]

    def validate(self, data):
        if data.get('date') is None:
            data['date'] = date.today()
        db_obj = DoctorDailyTime.objects.filter(date=data.get('date'))
        if db_obj:
            raise ValidationError(detail={'date': 'This date is already exist'})
        return data

    appointments = inline_serializer(many=True, read_only=True, fields={
        "id": serializers.IntegerField(),
        "appointment_date": serializers.DateTimeField(),
        "reserved_date": serializers.DateTimeField(),
        "status": serializers.CharField(),
        "patient": PatientSerializer(read_only=True),
    })
