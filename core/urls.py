from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from core import views

router = DefaultRouter()
router.register('doctor', views.DoctorDailyTimeView, 'doctor-time-view')
router.register('appointment', views.AppointmentView, 'appointment-view')

urlpatterns = router.urls
