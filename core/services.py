from dataclasses import dataclass
from datetime import timedelta, datetime, time

from django.core.exceptions import ObjectDoesNotExist, BadRequest

from core.models import Appointment, User, DoctorDailyTime, Patient


@dataclass
class DoctorDailyTimeDto:
    doctor: User
    start_time: time
    end_time: time
    date: datetime


class DoctorDailyTimeService:

    @staticmethod
    def get_doctor_daily_time_dto_from_validated_data(validated_data, doctor):

        data = validated_data
        return DoctorDailyTimeDto(
            start_time=data["start_time"],
            end_time=data["end_time"],
            date=data["date"],
            doctor=doctor
        )

    @staticmethod
    def create_appointments_with_doctor_time(dto: DoctorDailyTimeDto) -> DoctorDailyTime:
        """
        This function create appointments between start_time and end_time
        Each appointment set for 30 minutes
        """

        # Check if doctor enters an end date that is sooner than start date or not
        end_time = datetime.combine(dto.date, dto.end_time)
        start_time = datetime.combine(dto.date, dto.start_time)
        diff = end_time - start_time
        if diff < timedelta(minutes=30):
            raise BadRequest

        # Creating DoctorDailyTime
        doctor_dt_instance = DoctorDailyTime.objects.create(
            end_time=dto.end_time,
            start_time=dto.start_time,
            date=dto.date,
            doctor=dto.doctor,
        )

        # Create list of appointment between start_time and end_time
        appointment_list = []

        appointment_time = datetime.combine(dto.date, dto.start_time)
        time_check = datetime.combine(dto.date, dto.start_time)
        while True:
            time_check = appointment_time + timedelta(minutes=30)

            if time_check > end_time:
                break
            else:
                appointment_list.append(Appointment(appointment_date=appointment_time, doctor_time=doctor_dt_instance))
                appointment_time = appointment_time + timedelta(minutes=30)

        # Save created appointment to database
        Appointment.objects.bulk_create(appointment_list)

        return doctor_dt_instance


@dataclass
class CreatePatientDto:
    appointment_id: int
    phone_number: str
    name: str


class AppointmentService:

    @staticmethod
    def get_patient_dto_from_validated_data(validated_data):
        """Converting validated_data to CreatePatientDto  """
        data = validated_data
        return CreatePatientDto(
            appointment_id=data["appointment"].id,
            name=data["name"],
            phone_number=data["phone_number"],
        )

    @staticmethod
    def take_appointment_for_patient(dto: CreatePatientDto) -> Patient:
        """
        This function try to reserve appointment for patient

        """

        ap_db = Appointment.objects.filter(id=dto.appointment_id)
        # check the appointment is exists or not raise Exception
        if ap_db is None:
            raise ObjectDoesNotExist
        ap_db = ap_db.get()
        if ap_db.status == Appointment.RESERVED_STATUS:
            raise BadRequest

        if ap_db.status == Appointment.CLOSED_STATUS:
            raise ObjectDoesNotExist

        ap_db.status = Appointment.RESERVED_STATUS
        ap_db.reserved_date = datetime.now()
        ap_db.save()
        patient_obj = Patient.objects.create(
            appointment=ap_db,
            name=dto.name,
            phone_number=dto.phone_number
        )
        return patient_obj
