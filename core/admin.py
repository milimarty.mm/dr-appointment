from django.contrib import admin

# Register your models here.
from core.models import Patient, DoctorDailyTime, Appointment

admin.site.register(Patient)
admin.site.register(DoctorDailyTime)
admin.site.register(Appointment)
