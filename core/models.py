import django
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone

from core.validators import phone_validator


# Create your models here.

class User(AbstractUser):
    pass


class DoctorDailyTime(models.Model):
    doctor = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    date = models.DateField(default=django.utils.timezone.now, unique=True)
    start_time = models.TimeField()
    end_time = models.TimeField()

    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.doctor.username}  {self.date} "


class Appointment(models.Model):
    PENDING_STATUS = "P"
    RESERVED_STATUS = "R"
    CLOSED_STATUS = "C"

    STATUS = (
        (PENDING_STATUS, "pending"),
        (RESERVED_STATUS, "reserved"),
        (CLOSED_STATUS, "closed")
    )

    appointment_date = models.DateTimeField()
    reserved_date = models.DateTimeField(null=True, default=None)
    doctor_time = models.ForeignKey(DoctorDailyTime, on_delete=models.SET_NULL, null=True, related_name="appointments")
    status = models.CharField(choices=STATUS, max_length=1, default=PENDING_STATUS)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f" {self.id}  {self.status}"


class Patient(models.Model):
    appointment = models.OneToOneField(Appointment, related_name="patient", on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=100)
    phone_number = models.CharField(validators=[phone_validator()], max_length=17)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.name}  {self.phone_number}"
