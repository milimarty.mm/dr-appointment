import pytest
from model_bakery import baker
from rest_framework import status

from appointment import settings
from core.models import Appointment, DoctorDailyTime, Patient


@pytest.fixture
def create_appointments(api_client):
    def do_create_doctor_daily_time_and_appointments(doctor_dt):
        return api_client.post("/api/doctor/", doctor_dt)

    return do_create_doctor_daily_time_and_appointments


@pytest.mark.django_db
class TestCreateDoctorDailyTimeAndAppointments:
    def test_if_user_is_anonymous_return_401(self, create_appointments):
        response = create_appointments(
            {"start_time": "21:30", "end_time": "20:30", "date": "2022-06-05"}
        )

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_if_user_is_not_doctor_return_403(self, create_appointments, authenticate):
        authenticate()

        response = create_appointments(
            {"start_time": "21:30", "end_time": "20:30", "date": "2022-06-05"}
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_if_end_time_sooner_than_start_time_return_400(
            self, create_appointments, authenticate
    ):
        authenticate(is_staff=True)

        response = create_appointments(
            {"start_time": "21:30", "end_time": "20:30", "date": "2022-06-05"}
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_if_end_time_is_less_than_30_min_return_400(
            self, create_appointments, authenticate
    ):
        authenticate(is_staff=True)

        response = create_appointments(
            {"start_time": "20:30", "end_time": "20:15", "date": "2022-06-05"}
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_if_date_already_exist_return_400(self, create_appointments, authenticate):
        authenticate(is_staff=True)
        response = None
        for i in range(2):
            response = create_appointments(
                {"start_time": "20:30", "end_time": "20:15", "date": "2022-06-05"}
            )

        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_if_data_is_valid_return_201(self, authenticate, create_appointments):
        authenticate(True)

        response = create_appointments(
            {"start_time": "20:30", "end_time": "21:30", "date": "2022-06-08"}
        )
        print(response)
        assert response.status_code == status.HTTP_201_CREATED
        assert response.data["id"] > 0


@pytest.fixture
def doctor_appointment_list(api_client):
    def do_doctor_appointments(date=None, appointment_status=None):
        endpoint = "/api/doctor/?"
        if date:
            endpoint += f"date={date}&"
        if appointment_status:
            endpoint += f"status={appointment_status}"
        return api_client.get(endpoint)

    return do_doctor_appointments


@pytest.fixture
def current_doctor(authenticate):
    user = baker.make(settings.AUTH_USER_MODEL, is_staff=True)
    authenticate(user=user)

    doctor_dt = baker.make(DoctorDailyTime, doctor_id=user.id)
    return doctor_dt


@pytest.mark.django_db
class TestDoctorAppointmentList:
    def test_if_user_is_anonymous_return_401(self, doctor_appointment_list):
        response = doctor_appointment_list()

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_if_user_is_not_doctor_return_403(
            self, doctor_appointment_list, authenticate
    ):
        authenticate()

        response = doctor_appointment_list()
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_return_empty_list_if_no_appointment_taken(
            self, doctor_appointment_list, current_doctor
    ):
        appointments = baker.make(
            Appointment,
            _quantity=5,
            doctor_time=current_doctor,
            status=Appointment.PENDING_STATUS,
        )

        response = doctor_appointment_list(
            appointment_status=Appointment.RESERVED_STATUS
        )

        assert response.status_code == status.HTTP_200_OK
        assert len(response.data[0]["appointments"]) == 0

    def test_return_empty_list_if_no_appointment_open(
            self, doctor_appointment_list, current_doctor
    ):
        appointments = baker.make(
            Appointment,
            _quantity=5,
            doctor_time=current_doctor,
            status=Appointment.RESERVED_STATUS,
        )

        response = doctor_appointment_list(
            appointment_status=Appointment.PENDING_STATUS
        )

        assert response.status_code == status.HTTP_200_OK
        assert len(response.data[0]["appointments"]) == 0

    def test_if_patient_is_exist_in_taken_appointment(
            self, doctor_appointment_list, current_doctor
    ):
        appointments = baker.make(
            Appointment, doctor_time=current_doctor, status=Appointment.RESERVED_STATUS
        )
        patient = baker.make(Patient, appointment=appointments)
        response = doctor_appointment_list(
            appointment_status=Appointment.RESERVED_STATUS
        )

        assert response.status_code == status.HTTP_200_OK
        assert response.data[0]["appointments"][0]["id"] > 0


@pytest.fixture
def patient_appointments(api_client):
    def do_doctor_appointments(phone_number):
        endpoint = f"/api/appointment/?patient__phone_number={phone_number}"

        return api_client.get(endpoint)

    return do_doctor_appointments


@pytest.mark.django_db
class TestPatientAppointments:
    def test_if_there_is_no_patient_with_phone_number_return_empty_list(
            self, patient_appointments
    ):
        phone_number = "0911111111"

        appointments = baker.make(Appointment, status=Appointment.RESERVED_STATUS)
        patient = baker.make(
            Patient, appointment=appointments, phone_number=phone_number
        )

        response = patient_appointments(phone_number=phone_number + "2")

        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 0

    def test_show_all_patient_appointments(self, patient_appointments):
        phone_number = "0911111111"

        for i in range(2):
            appointments = baker.make(Appointment, status=Appointment.RESERVED_STATUS)
            patient = baker.make(
                Patient, appointment=appointments, phone_number=phone_number
            )

        response = patient_appointments(phone_number=phone_number)
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 2
