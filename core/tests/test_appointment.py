from datetime import datetime

import pytest
from model_bakery import baker
from rest_framework import status

from appointment import settings
from core.models import Appointment, DoctorDailyTime, Patient


@pytest.fixture
def delete_appointment(api_client):
    def do_delete_appointment(appointment_id=None):
        if appointment_id is None:
            appointment_id = 1
        return api_client.delete(f"/api/appointment/{appointment_id}/")

    return do_delete_appointment


@pytest.mark.django_db
class TestDeleteAppointments:

    def test_if_user_is_anonymous_return_401(self, delete_appointment):
        response = delete_appointment()

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_if_user_is_not_doctor_return_403(self, delete_appointment, authenticate):
        authenticate()

        response = delete_appointment()
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_if_appointment_not_found_return_404(self, delete_appointment, authenticate):
        authenticate(is_staff=True)

        response = delete_appointment()
        assert response.status_code == status.HTTP_404_NOT_FOUND

    def test_delete_taken_appointment_406(self, authenticate, delete_appointment):
        user = baker.make(settings.AUTH_USER_MODEL, is_staff=True)
        authenticate(user=user)

        doctor_dt = baker.make(DoctorDailyTime, doctor_id=user.id)

        appointments = baker.make(Appointment, doctor_time=doctor_dt)
        patient = baker.make(Patient, appointment=appointments)
        response = delete_appointment(appointments.id)

        assert response.status_code == status.HTTP_406_NOT_ACCEPTABLE

    def test_delete_appointment_return_204(self, delete_appointment, authenticate, ):
        user = baker.make(settings.AUTH_USER_MODEL, is_staff=True)
        authenticate(user=user)

        doctor_dt = baker.make(DoctorDailyTime, doctor_id=user.id)

        appointments = baker.make(Appointment, doctor_time=doctor_dt)
        response = delete_appointment(appointments.id)

        assert response.status_code == status.HTTP_204_NO_CONTENT


@pytest.fixture
def open_appointment(api_client):
    def open_appointment(appointment_id=None):
        return api_client.get("/api/appointment/")

    return open_appointment


@pytest.fixture
def doctor_daily_time():
    return baker.make(DoctorDailyTime, date=datetime.today())


@pytest.mark.django_db
class TestOpenAppointment:
    def test_empty_appointment_list_return_200(self, open_appointment, doctor_daily_time):
        appointments = baker.make(Appointment, _quantity=5, doctor_time=doctor_daily_time,
                                  status=Appointment.RESERVED_STATUS)
        appointments = baker.make(Appointment, _quantity=5, doctor_time=doctor_daily_time,
                                  status=Appointment.CLOSED_STATUS)

        response = open_appointment()
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 0

    def test_appointment_list_return_200(self, open_appointment, doctor_daily_time):
        appointments = baker.make(Appointment, _quantity=5, doctor_time=doctor_daily_time,
                                  status=Appointment.PENDING_STATUS)
        appointments = baker.make(Appointment, _quantity=5, doctor_time=doctor_daily_time,
                                  status=Appointment.CLOSED_STATUS)

        response = open_appointment()
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 5


@pytest.fixture
def take_appointment(api_client):
    def do_take_appointment(patient):
        return api_client.post("/api/appointment/", patient)

    return do_take_appointment


@pytest.mark.django_db
class TestTakeAppointment:

    def test_take_appointment_wit_invalid_data_400(self, take_appointment):
        appointments = baker.make(Appointment, _quantity=5,
                                  status=Appointment.PENDING_STATUS)

        response = take_appointment({
            "name": "",
            "phone_number": "",
            "appointment": appointments[0].id
        })
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.data.get("name")[0].code == "blank"
        assert response.data.get("phone_number")[0].code == "blank"

    def test_if_appointment_already_taken_return_409(self, take_appointment):
        appointments = baker.make(Appointment, _quantity=5,
                                  status=Appointment.PENDING_STATUS)
        response = None

        for i in range(2):
            response = take_appointment({
                "name": "Milad",
                "phone_number": "+989111111111",
                "appointment": appointments[0].id
            })
        appointment_list = Appointment.objects.filter(status=Appointment.RESERVED_STATUS).all()

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.data.get("appointment")[0].code == "unique"

    def test_take_appointment_201(self, take_appointment):
        appointments = baker.make(Appointment, _quantity=5,
                                  status=Appointment.PENDING_STATUS)
        response = take_appointment({
            "name": "Milad",
            "phone_number": "+989111111111",
            "appointment": appointments[0].id
        })
        appointment_list = Appointment.objects.filter(status=Appointment.RESERVED_STATUS).all()

        assert response.status_code == status.HTTP_201_CREATED
        assert response.data.get("id") > 0
        assert len(appointment_list) == 1
