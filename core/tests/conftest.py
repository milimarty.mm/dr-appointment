from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from model_bakery import baker
from rest_framework.test import APIClient
import pytest

from appointment import settings


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
@pytest.mark.django_db
def authenticate(api_client):
    def do_authenticate(is_staff=False, user=None):
        if user is None:
            user = baker.make(settings.AUTH_USER_MODEL, is_staff=is_staff)

        return api_client.force_authenticate(user=user)

    return do_authenticate
