from django.core.validators import RegexValidator


def phone_validator(error_message=None):
    if error_message is None:
        error_message = "Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed."
    return RegexValidator(regex=r'^\+?1?\d{9,15}$', message=error_message)
