from datetime import datetime, timedelta

from django.core.exceptions import ObjectDoesNotExist, BadRequest
from django.db.models import Q, Prefetch
from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from pip._internal.req.constructors import operators
from rest_framework import status
from rest_framework.mixins import CreateModelMixin, ListModelMixin, DestroyModelMixin, RetrieveModelMixin
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

# Create your views here.
from core.models import DoctorDailyTime, Patient, Appointment
from core.serializer import DoctorDailyTimeSerializer, AppointmentSerializer, CreatePatientSerializer
from core.services import DoctorDailyTimeDto, DoctorDailyTimeService, CreatePatientDto, AppointmentService
from utils.exceptions.api_exception import ApiRequestException


class DoctorDailyTimeView(ListModelMixin, CreateModelMixin, GenericViewSet):
    permission_classes = [IsAuthenticated, IsAdminUser]
    serializer_class = DoctorDailyTimeSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['date']

    def get_queryset(self):
        queryset = DoctorDailyTime.objects.all()

        appointment_status = self.request.query_params.get('status')
        if appointment_status:
            queryset = queryset.prefetch_related(
                Prefetch(
                    'appointments',
                    queryset=Appointment.objects.filter(status=appointment_status)
                ), 'appointments__patient')
        else:
            queryset = queryset.prefetch_related("appointments__patient")

        date = self.request.query_params.get('date')
        if not date:
            queryset = queryset.filter(Q(doctor=self.request.user) & Q(date=datetime.today()))

        return queryset

    def create(self, request, *args, **kwargs):
        """Create DoctorDailyTime Object with appointment between start and end time"""
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        doctor_dt_service = DoctorDailyTimeService()
        try:
            dto = doctor_dt_service.get_doctor_daily_time_dto_from_validated_data(serializer.validated_data,
                                                                                  doctor=self.request.user)
            doctor_dt_obj = doctor_dt_service.create_appointments_with_doctor_time(dto)
        except BadRequest:
            raise ApiRequestException(
                ApiRequestException.ExceptionEnum.END_TIME_IS_BIGGER_THAN_START_TIME_OR_END_TIME_IS_LESS_THAN_30_MIN)

        output_data = self.get_serializer(instance=doctor_dt_obj)
        return Response(output_data.data, status=status.HTTP_201_CREATED)


class AppointmentView(RetrieveModelMixin, ListModelMixin, DestroyModelMixin, CreateModelMixin, GenericViewSet):
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['patient__phone_number']

    def get_permissions(self):
        if self.action == 'destroy':
            self.permission_classes = [IsAdminUser, IsAuthenticated]
        return super().get_permissions()

    def get_serializer_class(self):
        if self.action == 'create':
            return CreatePatientSerializer
        return AppointmentSerializer

    def get_queryset(self):

        queryset = Appointment.objects.prefetch_related('patient')

        phone_number = self.request.query_params.get('patient__phone_number')
        if phone_number:
            return queryset.all()

        if self.action == 'retrieve':
            return queryset.all()

        if self.action == 'list':
            return queryset.filter(
                Q(doctor_time__date=datetime.today()) & Q(status=Appointment.PENDING_STATUS))

        return Appointment.objects.prefetch_related('patient').filter(doctor_time__doctor=self.request.user)

    def create(self, request, *args, **kwargs):
        """Take an appointment for a patient"""

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        appointment_service = AppointmentService()

        try:
            dto = appointment_service.get_patient_dto_from_validated_data(serializer.validated_data)
            patient_obj = appointment_service.take_appointment_for_patient(dto)
        except ObjectDoesNotExist:
            raise ApiRequestException(message=ApiRequestException.ExceptionEnum.NOT_FOUND,
                                      status_code=status.HTTP_404_NOT_FOUND)
        except BadRequest:
            raise ApiRequestException(ApiRequestException.ExceptionEnum.APPOINTMENT_ALREADY_TAKEN,
                                      status_code=status.HTTP_409_CONFLICT)

        output_data = self.get_serializer(instance=patient_obj)
        return Response(output_data.data, status=status.HTTP_201_CREATED)

    def destroy(self, request, *args, **kwargs):
        """this function delete an appointment"""

        instance = self.get_object()
        ap_obj = Patient.objects.filter(appointment=instance)

        if ap_obj:
            raise ApiRequestException(message=ApiRequestException.ExceptionEnum.NOT_ACCEPTABLE,
                                      status_code=status.HTTP_406_NOT_ACCEPTABLE)

        instance.status = Appointment.CLOSED_STATUS
        instance.save()

        return Response(status=status.HTTP_204_NO_CONTENT)
