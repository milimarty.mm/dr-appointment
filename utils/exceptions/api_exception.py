import datetime

from rest_framework.exceptions import APIException


class ApiRequestException(APIException):
    status_code = 400
    default_detail = 'Service temporarily unavailable, try again later.'
    default_code = 400

    def __init__(self, message, status_code=None):
        if status_code:
            self.status_code = status_code

        self.detail = {
            'status': self.status_code,
            'message': message,
            'timestamp': datetime.datetime.now().isoformat()
        }

    class ExceptionEnum:
        APPOINTMENT_ALREADY_TAKEN = "APPOINTMENT_ALREADY_TAKEN"
        END_TIME_IS_BIGGER_THAN_START_TIME_OR_END_TIME_IS_LESS_THAN_30_MIN = "END_TIME_IS_BIGGER_THAN_START_TIME_OR_END_TIME_IS_LESS_THAN_30_MIN"
        ILLEGAL = "ILLEGAL"
        NOT_ACCEPTABLE = "NOT_ACCEPTABLE"
        NOT_FOUND = "NOT_FOUND"
