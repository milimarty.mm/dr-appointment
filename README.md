# Conclusion
Solution one

In this solution, I tried to manage and organize appointments by creating
relation between `Appointments` and `DoctorDailyTimes` and we can easliy access to day appointments by retriving `DoctorDailyTimes` object. 
And it is so reliable for feature updates if we want to add another stuff.

![alt text](./model_1.png)


Solution two 

In this solution,there is no need to `DoctorDailyTime` we can calculate 
appointments and save them in one table, and we can make index for date, and easily we can take daily appointment only by searching date.
This way is so faster because there is no relation between tables.
But make our code more Complicates.

![alt text](./model_2.png)


# Another option for documentation
There is api documentation in this end point:
**URL:** `/swagger/` 



# Doctor Appointments

Return list of `DoctorDailyTime` objects that includes a list of `Appointment`.
If an `Appointment` is taken by the user Patient object would show its details.

* **URL**
	`/api/doctor/`
	
* **Method**
	`GET`
	
* **Query Params:** 
	`date=[Date] for example 2022-06-22`
	
	- If date object is empty it will return current date
	
	`status=[R|C|P] `
	
	- R = Reserved by user 
	- C = Closed by doctor 
	- P = pending to reserve


* **Error Response:**
	Code: 401 UNAUTHORIZED

OR
	Code: 403 FORBIDDEN

* **Success Response:**

  Code: 200
  Content: 
```json
  {
    "id": 1,
    "start_time": "17:37:00",
    "end_time": "22:38:00",
    "date": "2022-06-05",
    "appointments": [
        {
            "id": 1,
            "appointment_date": "2022-06-05T17:37:00Z",
            "reserved_date": null,
            "status": "P",
            "patient": null
        },
        {
            "id": 2,
            "appointment_date": "2022-06-05T18:07:00Z",
            "reserved_date": null,
            "status": "C",
            "patient": null
        },
        {
                "id": 3,
                "appointment_date": "2022-06-05T18:37:00Z",
                "reserved_date": "2022-06-05T18:09:25.048624Z",
                "status": "R",
                "patient": {
                    "id": 1,
                    "name": "Milad",
                    "phone_number": "09141234578"
                }
            },
        ...
  }
```





# Create Appointments

Return list of `DoctorDailyTime` objects that includes a list of `Appointment`

* **URL**
  `/api/doctor/`

* **Method**
  `POST`

* **Data Params**

  ```json
  {
      "start_time": "17:37:00",
      "end_time": "22:38:00",
      "date": "2022-06-05"
  }
  ```
  
* **Success Response:**
    - **Code:** 201 CREATED
      **Content:** 
      
      ``` json
      {
          "id": 2,
          "start_time": "11:00:00",
          "end_time": "12:01:00",
          "date": "2022-06-17",
          "appointments": [
              {
                  "id": 11,
                  "appointment_date": "2022-06-17T11:00:00Z",
                  "reserved_date": null,
                  "status": "P",
                  "patient": null
              },
              {
                  "id": 12,
                  "appointment_date": "2022-06-17T11:30:00Z",
                  "reserved_date": null,
                  "status": "P",
                  "patient": null
              }
          ]
      }
      ```
* **Error Response:**
	
	- Code: 401 UNAUTHORIZED
	OR
	- Code: 403 FORBIDDEN
	OR
	- code: 400 BAD REQUEST 
	- If date already exist 
	  content:
```json
	  
{
    "date": [
        "doctor daily time with this date already exists."
    ]
}
```
OR
	- code: 400 BAD REQUEST
	- if end time sooner than start time 
	 content:
```json
	{
    "status": 400,
    "message": 					"END_TIME_IS_BIGGER_THAN_START_TIME_OR_END_TIME_IS_LESS_THAN_30_MIN",
    "timestamp": "2022-06-05T18:37:16.441007"
}
```



# Take an appointment by user 

Return a `Patient` object that includes the `name`, `phone_number`, and `appointment_id`

* **URL**
  `/api/appointment/`

* **Method**
	`POST`
  
* **Data Params**

  ```json
  {
      "name": "Milad",
      "phone_number": "09141234567",
      "appointment": 1
  }
  ```

* **Success Response:**

  - **Code:** 201 CREATED
    **Content:** 
    
```json
{
    "id": 2,
    "name": "milad",
    "phone_number": "09141234567"
}
```

* **Error Response:**

	- **Code:** 400 BAD REQUEST
	- If the appointment already taken
  	content:
```json
{
    "appointment": [
        "This field must be unique."
    ]
}
```

OR 

* **Error Response:**

	- **Code:** 400 BAD REQUEST
	- If the `phone_number` or `name` field is empty:
  	content:
```json
{
    "phone_number": [
        "This field may not be blank."
    ],
    "appointment": [
        "This field must be unique."
    ]
}
```
OR
* **Error Response:**
	- **Code:** 400 BAD REQUEST
	- If the `phone_number` format  is incorrect:
		content:
```json
{
       "phone_number": [
        "Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed."
    ],
}
```



## Open appointment list and user appointment 

Return list of `Appointment` objects that.

* **URL**
  `/api/appointment/`

* **Method**
	`GET`
	
* **Query Params:** 
	- If you want to see user appointments pass this query param:
	 `patient__phone_number=[str] for exapmple 09141234567`
	- If `patient__phone_number` is empty you will get to days open appointments


* **Success Response:**

  - **Code:** 200 OK
    **Content:** 
    
```json
[
    {
        "id": 1,
        "appointment_date": "2022-06-05T17:37:00Z",
        "status": "P"
    },
    {
        "id": 2,
        "appointment_date": "2022-06-05T18:07:00Z",
        "status": "P"
    },
    ...
]
```



# Delete appointment

Return a `Patient` object that includes the `name`, `phone_number`, and `appointment_id`

* **URL**
  `/api/appointment/{appointmen_id}/`

* **Method**
  `DELETE`
  
* **Success Response:**
  - **Code:** 204 NO CONTENT

* **Error Response:**

  - **Code:** 404 NOT FOUND
    content:
  
    ```json
    {
        "detail": "Not found."
    }
    ```
    
  - **Code:** 401 UNAUTHORIZED
  
    OR
  
  - **Code:** 403** FORBIDDEN
  
  
